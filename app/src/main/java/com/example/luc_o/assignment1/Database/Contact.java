package com.example.luc_o.assignment1.Database;

/**
 * Created by luc-o on 01/03/2017.
 */

public class Contact {

    private int _id;
    private String _name;
    private String _familyName;
    private String _homePhone;
    private String _mobilePhone;
    private String _mail;

    public Contact(int id, String name, String familyName, String homePhone, String mobilePhone, String mail) {
        this._id = id;
        this._name = name;
        this._familyName = familyName;
        this._homePhone = homePhone;
        this._mobilePhone = mobilePhone;
        this._mail = mail;
    }

    public int getId() {
        return this._id;
    }

    public String getName() {
        return this._name;
    }

    public String getFamilyName() {
        return this._familyName;
    }

    public String getHomePhone() {
        return this._homePhone;
    }

    public String getMobilePhone() {
        return this._mobilePhone;
    }

    public String getMail() {
        return this._mail;
    }

    @Override
    public String toString() {
        return this._name + " " + this._familyName;
    }
}
