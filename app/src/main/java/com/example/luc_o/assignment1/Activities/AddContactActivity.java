package com.example.luc_o.assignment1.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luc_o.assignment1.Database.ContactHelper;
import com.example.luc_o.assignment1.R;

public class AddContactActivity extends AppCompatActivity {

    private EditText _nameEditText;
    private EditText _familyNameEditText;
    private EditText _homePhoneEditText;
    private EditText _mobilePhoneEditText;
    private EditText _mailEditText;
    private ContactHelper _ch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        this._ch = ContactHelper.getInstance(this);
        this._nameEditText = (EditText) findViewById(R.id.nameEditText);
        this._familyNameEditText = (EditText) findViewById(R.id.familyNameEditText);
        this._homePhoneEditText = (EditText) findViewById(R.id.homePhoneEditText);
        this._mobilePhoneEditText = (EditText) findViewById(R.id.mobilePhoneEditText);
        this._mailEditText = (EditText) findViewById(R.id.mailEditText);
    }

    public void okClicked(View v) {
        if (!checkContact())
            return ;
        if (this._ch.createContact(this, this._nameEditText.getText().toString(),
                this._familyNameEditText.getText().toString(),
                this._homePhoneEditText.getText().toString(),
                this._mobilePhoneEditText.getText().toString(),
                this._mailEditText.getText().toString())) {
            Intent intent = new Intent(AddContactActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private Boolean checkContact() {
        if (this._nameEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._familyNameEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid family name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._homePhoneEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid home phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._mobilePhoneEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid mobile phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._mailEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid mail", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
