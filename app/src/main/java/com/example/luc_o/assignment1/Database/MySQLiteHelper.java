package com.example.luc_o.assignment1.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by luc-o on 28/02/2017.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "contactsss";
    public static final String TABLE_NAME = "contacts";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_FAMILY_NAME = "family_name";
    public static final String COLUMN_HOME_PHONE = "home_phone";
    public static final String COLUMN_MOBILE_PHONE = "mobile_phone";
    public static final String COLUMN_MAIL = "mail";

    public static final String WHERE = null;
    public static final String WHERE_ARGS[] = null;
    public static final String GROUP_BY = null;
    public static final String HAVING = null;
    public static final String ORDER_BY = null;

    public static final String[] COLUMNS = {COLUMN_ID,
            COLUMN_NAME,
            COLUMN_FAMILY_NAME,
            COLUMN_HOME_PHONE,
            COLUMN_MOBILE_PHONE,
            COLUMN_MAIL
    };

    public MySQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory
            factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int version_old, int version_new) {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_FAMILY_NAME + " TEXT, "
            + COLUMN_HOME_PHONE + " TEXT, "
            + COLUMN_MOBILE_PHONE + " TEXT, "
            + COLUMN_MAIL + " TEXT)";

    private static final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;
}
