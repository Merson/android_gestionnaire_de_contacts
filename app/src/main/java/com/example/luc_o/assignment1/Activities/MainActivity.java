package com.example.luc_o.assignment1.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luc_o.assignment1.Database.Contact;
import com.example.luc_o.assignment1.Database.ContactAdapter;
import com.example.luc_o.assignment1.Database.ContactHelper;
import com.example.luc_o.assignment1.Database.MySQLiteHelper;
import com.example.luc_o.assignment1.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView _countTextView;
    private ContactHelper _ch;
    private ListView _contactListView;
    private ArrayList<Contact> _contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this._ch = ContactHelper.getInstance(this);
        this._contactListView = (ListView) findViewById(R.id.contactListView);
        this._countTextView = (TextView) findViewById(R.id.countTextView);
        this.showContacts();

        this._contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailsContactActivity.class);
                intent.putExtra("ID", _contacts.get(position).getId());
                intent.putExtra("NAME", _contacts.get(position).getName());
                intent.putExtra("FAMILY_NAME", _contacts.get(position).getFamilyName());
                intent.putExtra("HOME_PHONE", _contacts.get(position).getHomePhone());
                intent.putExtra("MOBILE_PHONE", _contacts.get(position).getMobilePhone());
                intent.putExtra("MAIL", _contacts.get(position).getMail());
                startActivity(intent);
            }
        });

        this._contactListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                System.out.println("I'M AT POSITION " + position);
                final long my_id = id;
                final int pos = position;
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("What do you want to do ?")
                        .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (_ch.deleteContact(_contacts.get(position).getId()))
                                    Toast.makeText(getBaseContext(), "Success", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getBaseContext(), "Fail", Toast.LENGTH_SHORT).show();
                                showContacts();
                            }
                        })
                        .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(MainActivity.this, EditContactActivity.class);
                                intent.putExtra("ID", _contacts.get(pos).getId());
                                intent.putExtra("NAME", _contacts.get(pos).getName());
                                intent.putExtra("FAMILY_NAME", _contacts.get(pos).getFamilyName());
                                intent.putExtra("HOME_PHONE", _contacts.get(pos).getHomePhone());
                                intent.putExtra("MOBILE_PHONE", _contacts.get(pos).getMobilePhone());
                                intent.putExtra("MAIL", _contacts.get(pos).getMail());
                                startActivity(intent);
                            }
                        })
                        .create()
                        .show();
                return false;
            }
        });
    }

    public void showContacts() {
        this._contacts = this._ch.getContacts();
        this._countTextView.setText(this._contacts.size() + " contacts found");
        final ContactAdapter ca = new ContactAdapter(this, this._contacts);
        this._contactListView.setAdapter(ca);
    }

    public void addContactClicked(View v) {
        Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
        startActivity(intent);
    }

    protected void onDestroy() {
        super.onDestroy();
        ContactHelper.getDatabase().delete(MySQLiteHelper.TABLE_NAME, MySQLiteHelper.WHERE, MySQLiteHelper.WHERE_ARGS);
    }

}
