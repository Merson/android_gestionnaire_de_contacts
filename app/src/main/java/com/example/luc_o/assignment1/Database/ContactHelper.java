package com.example.luc_o.assignment1.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luc-o on 28/02/2017.
 */

public class ContactHelper {

    private static SQLiteDatabase _sdb;
    private static ContactHelper _instance;

    public static ContactHelper getInstance(Context context) {
        if (_instance == null) {
            System.out.println("INSTANTIATE CONTACT HELPER");
            _instance = new ContactHelper(context);
        }
        System.out.println("ALREADY INSTANCIATED");
        return _instance;
    }

    private ContactHelper(Context context) {
        MySQLiteHelper tdb = new MySQLiteHelper(context, MySQLiteHelper.DATABASE_NAME, null, MySQLiteHelper.DATABASE_VERSION);
        _sdb = tdb.getWritableDatabase();
    }

    public Boolean createContact(Context context, String name, String familyName, String homePhone, String mobilePhone, String mail) {
        System.out.println(MySQLiteHelper.CREATE_TABLE);
        Boolean success = false;
        ContentValues cv = new ContentValues();
        cv.put(MySQLiteHelper.COLUMN_NAME, name);
        cv.put(MySQLiteHelper.COLUMN_FAMILY_NAME, familyName);
        cv.put(MySQLiteHelper.COLUMN_HOME_PHONE, homePhone);
        cv.put(MySQLiteHelper.COLUMN_MOBILE_PHONE, mobilePhone);
        cv.put(MySQLiteHelper.COLUMN_MAIL, mail);
        if (checkCreateContact(name))
            success = _sdb.insert(MySQLiteHelper.TABLE_NAME, null, cv) > 0;
        else
            Toast.makeText(context, "This user already exist !", Toast.LENGTH_SHORT).show();
        return success;
    }

    private Boolean checkCreateContact(String name) {
        ArrayList<Contact> contacts = this.getContacts();
        for (int i = 0; i < contacts.size(); ++i) {
            if (contacts.get(i).getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    public Boolean updateContact(int id, String name, String familyName, String homePhone, String mobilePhone, String mail) {
        final String WHERE = "id = ?";
        final String[] WHERE_ARGS = {Integer.toString(id)};
        Boolean success = false;
        ContentValues cv = new ContentValues();
        cv.put(MySQLiteHelper.COLUMN_NAME, name);
        cv.put(MySQLiteHelper.COLUMN_FAMILY_NAME, familyName);
        cv.put(MySQLiteHelper.COLUMN_HOME_PHONE, homePhone);
        cv.put(MySQLiteHelper.COLUMN_MOBILE_PHONE, mobilePhone);
        cv.put(MySQLiteHelper.COLUMN_MAIL, mail);
        success = _sdb.update(MySQLiteHelper.TABLE_NAME, cv, WHERE, WHERE_ARGS) > 0;
        return success;
    }

    public Boolean deleteContact(int id) {
        System.out.println("DELETE FROM " + MySQLiteHelper.TABLE_NAME + " WHERE " + MySQLiteHelper.COLUMN_ID + "=" + (id));

        return this._sdb.delete(MySQLiteHelper.TABLE_NAME,
                MySQLiteHelper.COLUMN_ID + "=?",
                new String[] {String.valueOf(id)}) > 0;
    }

    public ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Cursor c = _sdb.query(MySQLiteHelper.TABLE_NAME,
                MySQLiteHelper.COLUMNS,
                MySQLiteHelper.WHERE,
                MySQLiteHelper.WHERE_ARGS,
                MySQLiteHelper.GROUP_BY,
                MySQLiteHelper.HAVING,
                MySQLiteHelper.ORDER_BY);
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            Contact tmp = new Contact(c.getInt(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5));
            contacts.add(tmp);
            c.moveToNext();
        }
        return contacts;
    }

    public void showContacts() {
        Cursor c = _sdb.query(MySQLiteHelper.TABLE_NAME,
                MySQLiteHelper.COLUMNS,
                MySQLiteHelper.WHERE,
                MySQLiteHelper.WHERE_ARGS,
                MySQLiteHelper.GROUP_BY,
                MySQLiteHelper.HAVING,
                MySQLiteHelper.ORDER_BY);
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            System.out.println(c.getInt(0) + " "
                    + c.getString(1) + " "
                    + c.getString(2) + " "
                    + c.getString(3) + " "
                    + c.getString(4) + " "
                    + c.getString(5));
            c.moveToNext();
        }
    }

    public static SQLiteDatabase getDatabase() {
        return _sdb;
    }
}
