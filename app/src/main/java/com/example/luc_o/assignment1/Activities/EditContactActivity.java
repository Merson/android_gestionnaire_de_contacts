package com.example.luc_o.assignment1.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.luc_o.assignment1.Database.ContactHelper;
import com.example.luc_o.assignment1.R;

public class EditContactActivity extends AppCompatActivity {

    private EditText _nameEditText;
    private EditText _familyNameEditText;
    private EditText _homePhoneEditText;
    private EditText _mobilePhoneEditText;
    private EditText _mailEditText;
    private ContactHelper _ch;
    private Bundle _extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        this._ch = ContactHelper.getInstance(this);
        this._nameEditText = (EditText) findViewById(R.id.nameEditText);
        this._familyNameEditText = (EditText) findViewById(R.id.familyNameEditText);
        this._homePhoneEditText = (EditText) findViewById(R.id.homePhoneEditText);
        this._mobilePhoneEditText = (EditText) findViewById(R.id.mobilePhoneEditText);
        this._mailEditText = (EditText) findViewById(R.id.mailEditText);
        this._extras = getIntent().getExtras();

        System.out.println(this._extras.getString("NAME"));

        this._nameEditText.setText(this._extras.getString("NAME"));
        this._familyNameEditText.setText(this._extras.getString("FAMILY_NAME"));
        this._homePhoneEditText.setText(this._extras.getString("HOME_PHONE"));
        this._mobilePhoneEditText.setText(this._extras.getString("MOBILE_PHONE"));
        this._mailEditText.setText(this._extras.getString("MAIL"));
    }

    public void okEditClicked(View v) {
        if (!checkContact())
            return;
        this._ch.updateContact(
                this._extras.getInt("ID"),
                this._nameEditText.getText().toString(),
                this._familyNameEditText.getText().toString(),
                this._homePhoneEditText.getText().toString(),
                this._mobilePhoneEditText.getText().toString(),
                this._mailEditText.getText().toString());
        Intent intent = new Intent(EditContactActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private Boolean checkContact() {
        if (this._nameEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._familyNameEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid family name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._homePhoneEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid home phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._mobilePhoneEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid mobile phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this._mailEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Enter a valid mail", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
