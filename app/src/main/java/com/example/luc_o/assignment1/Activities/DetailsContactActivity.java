package com.example.luc_o.assignment1.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.luc_o.assignment1.Database.Contact;
import com.example.luc_o.assignment1.Database.ContactHelper;
import com.example.luc_o.assignment1.R;

public class DetailsContactActivity extends AppCompatActivity {

    private TextView _nameTextView;
    private TextView _familyNameTextView;
    private TextView _homePhoneTextView;
    private TextView _mobilePhoneTextView;
    private TextView _mailTextView;
    private ContactHelper _ch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_contact);
        this._ch = ContactHelper.getInstance(this);
        this._nameTextView = (TextView) findViewById(R.id.nameTextView);
        this._familyNameTextView = (TextView) findViewById(R.id.familyNameTextView);
        this._homePhoneTextView = (TextView) findViewById(R.id.homePhoneTextView);
        this._mobilePhoneTextView = (TextView) findViewById(R.id.mobilePhoneTextView);
        this._mailTextView = (TextView) findViewById(R.id.mailTextView);

        Bundle extras = getIntent().getExtras();

        this._nameTextView.setText("Name : " + extras.getString("NAME"));
        this._familyNameTextView.setText("Family name : " + extras.getString("FAMILY_NAME"));
        this._homePhoneTextView.setText("Home phone : " + extras.getString("HOME_PHONE"));
        this._mobilePhoneTextView.setText("Mobile phone : " + extras.getString("MOBILE_PHONE"));
        this._mailTextView.setText("Mail : " + extras.getString("MAIL"));
    }
}
