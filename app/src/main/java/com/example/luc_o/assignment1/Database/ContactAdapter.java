package com.example.luc_o.assignment1.Database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luc_o.assignment1.R;

import java.util.ArrayList;

/**
 * Created by luc-o on 01/03/2017.
 */

public class ContactAdapter extends BaseAdapter {

    private static class ContactViewHolder {
        private ImageView _contactView;
        private TextView _contactName;
    }

    private Context _context;
    private ArrayList<Contact> _contacts;
    private ContactViewHolder _cvh;

    public ContactAdapter(Context context, ArrayList<Contact> contact) {
        this._context = context;
        this._contacts = contact;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            this._cvh = new ContactViewHolder();
            LayoutInflater inflator = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.item_contact, parent, false);
            this._cvh._contactView = (ImageView) convertView.findViewById(R.id.contactImageView);
            this._cvh._contactName = (TextView) convertView.findViewById(R.id.contactName);
            convertView.setTag(this._cvh);
        } else {
            this._cvh = (ContactViewHolder) convertView.getTag();
        }
        this._cvh._contactName.setText(this._contacts.get(position).getName() + " " + this._contacts.get(position).getFamilyName());
        return convertView;
    }

    public int getCount() { return this._contacts.size(); }

    public long getItemId(int position) { return position; }

    public Object getItem(int position) { return this._contacts.get(position); }
}
